/**
 * Created by Gabo on 17/4/2018.
 */
public class Figura {
    private int nlados;
    private String nombre;
    private String descripcion;

    public Figura(int nlados, String nombre, String descripcion) {
        super();
        this.nlados = nlados;
        this.nombre = nombre;
        this.descripcion = descripcion;
        Logger.getInstance().writeLog("creando figura con " + nlados + " lados");
        Logger.getInstance().writeLog("creando figura: " + nombre + " y su descripcion es: " + descripcion);

    }

    public int getNlados(){
        Logger.getInstance().writeLog("numero de lados: " + nlados);
        return nlados;
    }

    public String getNombre(){
        Logger.getInstance().writeLog("Nombre figura: "+nombre);
        return nombre;
    }
}
