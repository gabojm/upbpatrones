/**
 * Created by Gabo on 17/4/2018.
 */
public class Persona {
    private String nombre;
    private int edad;
    private int altura;

    public Persona(String nombre, int edad, int altura) {
        super();
        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura;
        Logger.getInstance().writeLog("creando persona llamda " + nombre + " tiene " + edad + " años y su altura es " + altura);
    }

    public String getNombre(){
        Logger.getInstance().writeLog("Nombre persona: "+nombre);
        return nombre;
    }

    public int getEdad(){
        Logger.getInstance().writeLog("Edad: " +edad);
        return edad;
    }
}
