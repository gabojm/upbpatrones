/**
 * Created by Gabo on 17/4/2018.
 */
public class Logger {
    private static Logger ourInstance = null;

    private String allLog="";

    public static Logger getInstance() {
        if (ourInstance == null)
            ourInstance = new Logger();

        return ourInstance;
    }

    public void writeLog(String log) {
        allLog=allLog+log+"\n";
    }

    public void show() {
        System.out.println(allLog);
    }

    private Logger() {

    }

}
