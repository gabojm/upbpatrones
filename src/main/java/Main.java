/**
 * Created by Gabo on 17/4/2018.
 */
public class Main {

    public static void main(String[] args) {
        Figura f = new Figura(4,"cuadrado","tiene 4 lados");
        f.getNlados();
        f.getNombre();

        Persona p = new Persona("Juan",20,180);
        p.getNombre();
        p.getEdad();

        Logger.getInstance().show();
    }
}
